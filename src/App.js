import React from 'react';
import {
  BrowserRouter,
  HashRouter,
  Switch,
  Route,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import dayjs from 'dayjs';

import store from './store';

import {
  Home,
  useCallbackPage,
  useStatePage,
  useMemoPage,
  useEffectPage,
  useCustomPage,
  testSagaPage,
} from './view';

global.dayjs = dayjs;

const Router = process.env.BASE_PATH ? HashRouter : BrowserRouter;

export default () => (
  <Provider store={store}>
    <Router basename={process.env.BASE_PATH}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/useState" component={useStatePage} />
        <Route exact path="/useCallback" component={useCallbackPage} />
        <Route exact path="/useMemo" component={useMemoPage} />
        <Route exact path="/useEffect" component={useEffectPage} />
        <Route exact path="/useCustom" component={useCustomPage} />
        <Route exact path="/testSaga" component={testSagaPage} />
      </Switch>
    </Router>
  </Provider>
);