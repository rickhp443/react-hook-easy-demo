/* eslint-disable react/display-name */
import React, {
  useState,
  useCallback,
  memo,
} from 'react';

import dayjs from 'dayjs';

let count1 = 0;
let count2 = 0;

// 搭配useCallback
const Block1 = memo((props) => {
  console.log(`=== Block1 rerender ${dayjs().format().slice(0, 19)} ===`);
  count1 = count1 + 1;
  /* eslint-disable react/prop-types */
  const {onChangeCb} = props;

  return (
    <>
      執行Block1次數：{ count1 }<br/>
      input1輸入:<input type="text"
      onChange={(e) => onChangeCb(e)}/>
    </>
  );
});

const Block2 = (props) => {
  console.log(`=== Block2 rerender ${dayjs().format().slice(0, 19)} ===`);
  count2 = count2 + 1;
  return (
    <>
      執行Block2次數：{ count2 }<br/>
      input2輸入:<input type="text"
      onChange={(e) => props.onChangeCb(e)}/>
    </>
  );
};

export default () => {
  const [ input1, setInput1 ] = useState('');
  const [ input2, setInput2 ] = useState('');
  // const [ input3, setInput3 ] = useState('');

  // 使用memo的 要用
  const handleInput1ChangeCb = useCallback((e) => {
    setInput1(e.target.value);
  }, [input1]);

  /* 沒用memo就不要用
   * 同等
   * const handleInput2ChangeCb = (e) => {
   *   setInput2(e.target.value);
   * };
   */
  const handleInput2ChangeCb = useCallback((e) => {
    setInput2(e.target.value);
  }, [input2]);


  return (
    <div className="App" style={{ width: 500, margin: 'auto' }}>
      <a href="/">Home</a>
      <h1>useCallback(memo),請開啟瀏覽器開發者工具</h1>
      <h2>Block1 - 不會被其他影響到</h2>
      <p>input1的值：{ input1 }</p>
      <Block1 onChangeCb={handleInput1ChangeCb}/>
      <hr/>

      <h1>Block2 - 會被其他影響到</h1>
      <p>input2的值：{ input2 }</p>
      <Block2 onChangeCb={handleInput2ChangeCb}/>
      <hr/>

      <p>
        參考：
        <a
        // eslint-disable-next-line react/jsx-no-target-blank
        target="_blank"
        href="https://www.jianshu.com/p/188062c90fe9">
          47.React学习笔记.hooks useCallback
        </a>
      </p>

    </div>
  );
};