import React, {
  useState,
} from 'react';


export default () => {
  const [ countA, setCountA ] = useState(0);
  const [ countB, setCountB ] = useState(0);

  const clickAEvent = () => {
    setCountA(countA+1);
  };

  const clickBEvent = () => {
    setCountB(pre => pre + 1);
  };

  return (
    <div style={{ margin: 'auto', width: 500, }}>
      <a href="/">Home</a>
      <h1>useState,請開啟瀏覽器開發者工具</h1>
      <p>setCountA(countA+1);</p>
      { countA }
      <button onClick={clickAEvent}>click = a</button><br/>

      <p>setCountB(pre =&gt; pre + 1);</p>
      { countB }
      <button onClick={clickBEvent}>click = b</button><br/>

      <hr/>

      <p>
        參考：
        <a
        target="_blank"
        href="https://medium.com/@xyz030206/%E9%97%9C%E6%96%BC-usestate-%E4%BD%A0%E9%9C%80%E8%A6%81%E7%9F%A5%E9%81%93%E7%9A%84%E4%BA%8B-5c8c4cdda82c">
          關於 useState，你需要知道的事
        </a>
      </p>
    </div>
  );
};