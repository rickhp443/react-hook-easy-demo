import React, {
  useEffect,
  useLayoutEffect,
  useState,
} from 'react';

export default () => {
  
  const [ state, setState ] = useState(0);

  const [ init, setInit ] = useState(true);

  useEffect(() => {
    console.log('componentDidMount');
  }, []);

  useEffect(() => {
    console.log('each times');
    if(init) {
      console.log('init');
      setInit(false);
    }
  });

  useLayoutEffect(() => {
    console.log('useLayoutEffect');
  });

  

  return (
    <div className="App" style={{ width: 500, margin: 'auto' }}>
      <a href="/">Home</a>
      <h1>useEffect / useLayoutEffect,請開啟瀏覽器開發者工具</h1>
      { state }
      <button onClick={() => setState(pre => pre + 1)}>+</button>
      
      <hr/>
      <p>
        useEffect: 異步，在畫面 render 結束後才執行，不會阻塞 UI<br/>
        useLayoutEffect: 同步，在 DOM 改變後馬上執行，會阻塞 UI
        <br/><br/>
        useLayoutEffect 跟 class 的 componentDidMount、componentDidUpdate、componentWillUnmount 完全一樣
      </p>
      <p>
        參考：
        <a
        target="_blank"
        href="https://juejin.im/post/6844904008402862094">
          useEffect 和 useLayoutEffect 的区别
        </a>
        <a
        target="_blank"
        href="https://medium.com/@felippenardi/how-to-do-componentdidmount-with-react-hooks-553ba39d1571">
          How to do componentDidMount with React Hooks?
        </a>
      </p>
    </div>
  );
};