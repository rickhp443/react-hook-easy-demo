import React from 'react';
import { connect } from 'react-redux';
import {
  setTest2Action,
  setTest3Action,
  getTest1Action,
  another1,
  another2,
  another3,
} from '../actions/test';

const testSagaPage = (props) => {
  const {
    test1 = null,
    test2 = null,
    test3 = null,
    another1 = null,
    another2 = null,
    another3 = null,
  } = props;

  const click1 = () => {
    console.log('click1');
    props.getTest1Action();
  };
  const click2 = () => {
    console.log('click2');
    props.setTest2Action('abc');
  };
  const click3 = () => {
    console.log('click3');
    props.setTest3Action('ddd');
  };
  const click1a = () => {
    console.log('click1a');
    props.setAnother1();
  };
  const click2a = () => {
    console.log('click2a');
    props.setAnother2();
  };
  const click3a = () => {
    console.log('click3a');
    props.setAnother3();
  };
  return (
    <div className="App" style={{ width: 500, margin: 'auto' }}>
      <div>
        test1:
        { test1 }
        <button type="button" onClick={() => click1()}>test1</button>
      </div>
      <div>
        test2:
        { test2 }
        <button type="button" onClick={() => click2()}>test2</button>
      </div>
      <div>
        test3:
        { test3 }
        <button type="button" onClick={() => click3()}>test3</button>
      </div>
      <hr/>
      <div>
        another1:
        { another1 }
        <button type="button" onClick={() => click1a()}>another1</button>
      </div>
      <div>
        another2:
        { another2 }
        <button type="button" onClick={() => click2a()}>another2</button>
      </div>
      <div>
        another3:
        { another3 }
        <button type="button" onClick={() => click3a()}>another3</button>
      </div>
    </div>
  );
};

const mapStateToProps = ({ test, another }) => {
  const { test1, test2, test3 } = test;
  const { another1, another2, another3 } = another;
  return {
    test1,
    test2,
    test3,
    another1,
    another2,
    another3,
  };
};

const mapDispatchToProps = dispatch => ({
  getTest1Action() {dispatch(getTest1Action())},
  setTest2Action(payload) { dispatch(setTest2Action(payload))},
  setTest3Action(payload) { dispatch(setTest3Action(payload))},
  setAnother1() { dispatch(another1()) },
  setAnother2() { dispatch(another2()) },
  setAnother3() { dispatch(another3()) },
});

export default connect(mapStateToProps, mapDispatchToProps)(testSagaPage);
