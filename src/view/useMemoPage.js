import React, {
  useState,
  useMemo,
} from 'react';

export default () => {

  const [ firstName, setFirstName ] = useState('');
  const [ lastName, setLastName ] = useState('');

  const renderFullName = useMemo(() => {
    return `${firstName} ${lastName}`
  }, [
    firstName,
    lastName
  ]);

  return (
    <div style={{ margin: 'auto', width: 500, }}>
      <a href="/">Home</a>
      <h1>useMemo,請開啟瀏覽器開發者工具</h1>
      <h2>{renderFullName}</h2>
      firstName:<input type="text" value={firstName} onChange={(e) => setFirstName(e.target.value)}/><br/>
      lastName:<input type="text" value={lastName} onChange={(e) => setLastName(e.target.value)}/><br/>
    
      <button onClick={() => alert(renderFullName)}>確定</button>
      <hr/>

      <p>
        類似Vue的computed
        <br/>
        參考：
        <a
        target="_blank"
        href="https://juejin.im/post/6844903981819379719">
          乾坤大挪移！React 也能 “用上” computed 属性
        </a>
      </p>
    </div>
  );
};