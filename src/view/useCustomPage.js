import React, {
  useEffect,
  useState,
} from 'react';

const useTimer = (second, tick) => {
  const [ arrive, setArrive ] = useState(false);


  useEffect(() => {
    if(arrive && !tick) {
      setArrive(false);
    }
  });

  useEffect(() => {
    if(tick) {
      setTimeout(() => {
        setArrive(() => true);
      }, second*1000);
    }
  }, [tick]);
  

  return arrive;
};

export default () => {
  const [ start, setStart ] = useState(false);
  const [ click, setClick ] = useState(false);
  const arrive = useTimer(5, start);
  
  return (
    <div className="App" style={{ width: 500, margin: 'auto' }}>
      <a href="/">Home</a>
      <h1>useTimer(自製hook)</h1>
      {
        arrive ? (
          <>五秒到了!!，按下reset可回復原狀</>
        ) : (
          <>
            {
              start ? (
                <>計時中</>
              ) : (
                <>按下start開始計時</>
              )
              
            }
          </>
          
        )
      }
      <button onClick={() => {
        setClick(true);
        setStart(true);
      }}>start</button>

      <button onClick={() => {
        
        setStart(false);
      }}>reset</button> 

      <hr/>

      <p>
        參考：
        <a
        target="_blank"
        href="https://ithelp.ithome.com.tw/articles/10224881">
          【React.js入門 - 24】 Custom hook - 給我另一個超推React hook的理由
        </a>
      </p>

    </div>
  );
};