import React from 'react';

export default () => (
  <>
    <ul>
      <li>
        <a href="/useCallback">useCallback</a> - useCallback, memo
      </li>
      <li>
        <a href="/useState">useState</a> - useState
      </li>
      <li>
        <a href="/useMemo">useMemo</a> - useMemo
      </li>
      <li>
        <a href="/useEffect">useEffect</a> - useEffect, useLayoutEffect
      </li>
      <li>
        <a href="/useCustom">useCustom</a> - Custom hook
      </li>
      <li>
        <a href="/testSaga">testSaga</a> - test Saga
      </li>
    </ul>
    Home
    <p>
        參考：
        <a
        target="_blank"
        href="https://medium.com/@mts40110/react-hooks-%E4%B8%80%E4%BA%9B%E7%B4%80%E9%8C%84-e5476075d9b8">
          React Hooks 一些紀錄
        </a>
      </p>

  </>
);