import {
  all,
  fork,
  put,
  takeLatest,
  select,
} from 'redux-saga/effects';
import {
  setTest2,
  setTest3,
} from '../actions/reducerTest';
import { SET_TEST2_ACTION, SET_TEST3_ACTION } from '../actions/types';

function* changeTest2({ payload }) {
  console.log('saga SET_TEST2_ACTION');
  const test = yield select(state => state.test);
  console.log('test => ', test);
  yield put(setTest2(payload));
}

function* changeTest3({ payload }) {
  console.log('saga SET_TEST3_ACTION');
  console.log(payload);
  try {
    yield put(setTest3(`${payload} 3`));
  } catch (err) {
    console.error('error');
  }
}

function* setTest2Take() {
  yield takeLatest(SET_TEST2_ACTION, changeTest2);
}

function* setTest3Take() {
  yield takeLatest(SET_TEST3_ACTION, changeTest3);
}

export default function* testSaga() {
  yield all([
    fork(setTest2Take),
    fork(setTest3Take),
  ]);
}
