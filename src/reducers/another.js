import {
  ANOTHER1,
  ANOTHER2,
  ANOTHER3
} from '../actions/types'

const initState = {
  another1: 'another1',
  another2: 'another2',
  another3: 'another3',
};

export default (state = initState, action) => {
  
  switch (action.type) {
    case ANOTHER1:
      return {
        ...initState,
        another1: '觸發',
      };
    case ANOTHER2:
      return {
        ...initState,
        another2: '觸發',
      };
    case ANOTHER3:
      return {
        ...initState,
        another3: '觸發',
      };
    default:
      return state;
  }
};
