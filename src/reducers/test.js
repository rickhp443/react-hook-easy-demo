import {
  TEST1_ACTION,
  SET_TEST2,
  SET_TEST3,
} from '../actions/types';

const initState = {
  test1: 'test1初始值',
  test2: 'test2初始值',
  test3: 'test3初始值',
};

export default (state = initState, action) => {
  const { payload } = action;
  switch (action.type) {
    case TEST1_ACTION:
      console.log('reducer === TEST1_ACTION');
      return {
        ...state,
      };
    case SET_TEST2:
      console.log('reducer === SET_TEST2');
      console.log(state);
      console.log(payload);
      return {
        ...state,
        test2: payload,
      };
    case SET_TEST3:
      console.log('reducer === SET_TEST3');
      console.log(state);
      console.log(payload);
      return {
        ...state,
        test3: payload,
      };
    default:
      console.log('reducer === default');
      return state;
  }
};
