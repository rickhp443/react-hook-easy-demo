import {
  combineReducers,
} from 'redux';
import test from './test';
import another from './another';

export default combineReducers({
  test,
  another,
});
