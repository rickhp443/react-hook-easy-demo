
import {
  SET_TEST2,
  SET_TEST3,
} from './types';


export const setTest2 = (payload) => {
  console.log('setTest2');
  return {
    type: SET_TEST2,
    payload,
  }
};

export const setTest3 = (payload) => {
  console.log('setTest3');
  console.log(payload);
  return {
    type: SET_TEST3,
    payload,
  }
};
