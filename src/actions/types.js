module.exports = {
  TEST1_ACTION: 'test1',
  SET_TEST2_ACTION: 'change_test2',
  SET_TEST3_ACTION: 'change_test3',

  SET_TEST2: 'SET_TEST2',
  SET_TEST3: 'SET_TEST3',


  ANOTHER1: 'ANOTHER1',
  ANOTHER2: 'ANOTHER2',
  ANOTHER3: 'ANOTHER3',
};