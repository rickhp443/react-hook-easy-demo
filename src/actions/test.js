import {
  TEST1_ACTION,
  SET_TEST2_ACTION,
  SET_TEST3_ACTION,
  ANOTHER1,
  ANOTHER2,
  ANOTHER3
} from './types';

export const getTest1Action = () => {
  console.log('getTest1Action');
  return {
    type: TEST1_ACTION,
  }
};

export const setTest2Action = (payload) => {
  console.log('setTest2Action');
  return {
    type: SET_TEST2_ACTION,
    payload,
  }
};

export const setTest3Action = (payload) => {
  console.log('setTest3Action');
  return {
    type: SET_TEST3_ACTION,
    payload,
  }
};

export const another1 = () => {
  console.log('another1');
  return {
    type: ANOTHER1,
  };
};

export const another2 = () => {
  console.log('another2');
  return {
    type: ANOTHER2,
  };
};

export const another3 = () => {
  console.log('another3');
  return {
    type: ANOTHER3,
  };
};